#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  xmlTvIdFromChanName.py
#
#  Copyright 2014 Jonas <jonas@U36SD-jonas>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

__title__ ="xmlTvIdFromChanName4mythtv";
__author__="Jonas Fourquier" #alias SnouF de mythtv-fr.org
__version__="trunk"
__usage__ ='''
%(title)s
Version: %(version)s
Author: %(author)s

Introduced sheep in MythTV with the names of the TV channels read in a
xmltv file

Usage: %(file)s [option] xmltvfile

xmltvfile               a xmltv file

Options:
  -h, --help            show this help message and exit
  -r, --replace         replaces the existing xmltvid (by defaut introduces
                        xmltv only if there not have)
  -s:, --sourceid=      Update xmltvid only for this sourceid (by default
                        search xmltvid for all sources
'''%{'title':__title__, 'file':__file__, 'version':__version__,'author':__author__}


import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from getopt import getopt
from xml.etree.cElementTree import ElementTree
et = ElementTree()
from MythTV import MythDB
cr = MythDB().cursor()
try :
    from Levenshtein import distance as levenshtein
except :
    moduleLevenshtein = False
else :
    moduleLevenshtein = True
    import re


opts,args = getopt(sys.argv[1:], "hrs:",["help","replace","sourceid="])

opt_all = False
sourceid = False
for opt, arg in opts:
    if opt in ("-h", "--help"):
        sys.stdout.write(__usage__)
        sys.exit(0)
    elif opt in ("-r", "--replace"):
        opt_all = True
    elif opt in ("-s", "--sourceid"):
        if arg.isdigit() :
            sourceid = arg
        else :
            sys.stdout.write("Error: sourceid must be a numeric.\n")
            sys.exit(1)

if len(args) != 1:
    sys.stdout.write("Error: must be indicate a xmltv.\n--help for more information\n")
    print args
    sys.exit(1)

if not moduleLevenshtein :
    sys.stdout.write("WARNING: module Levenshtein no found.\n")

xmltvFile = args[0]
xmltvTree = et.parse(xmltvFile)
channels = {}
whereClause = ''

if not opt_all :
    whereClause += 'AND xmltvid = ""'

if sourceid :
    whereClause += 'AND sourceid = '+sourceid

# Attribut les xmltv par une regexp
for xmltvChannel in xmltvTree.findall('channel') :
    xmltvid = xmltvChannel.attrib['id']
    chanName = xmltvChannel.find('display-name').text
    cr.execute('SELECT chanid,name,xmltvid FROM channel WHERE name REGEXP "^'+chanName+'(( HD)|(HD)|( [(].*[)])){0,1}$"' + whereClause) #REXEXP: ignoring suffix "HD" or "(a string)"
    for chanId,chanName,oldxmltvid in cr.fetchall() :
        if channels.has_key(chanId) :
            sys.stdout.write("A xmltvid had already been found for %s ​​channel.\nReplace xmltvid %s by %s ? (y/n, default: yes)\n"%(chanName,oldxmltvid,xmltvid))
            if raw_input().lower() in ('n','no') :
                sys.stdout.write("%s keeped\n"%oldxmltvid)
            else :
                sys.stdout.write("%s replaced by %s\n"%(oldxmltvid,xmltvid))
                channels[chanId] = xmltvid
        else :
            channels[chanId] = xmltvid

# insert les xmltvids
for chanId,xmltvid in channels.items() :
    cr.execute('UPDATE  channel SET xmltvid = "'+xmltvid+'" WHERE  chanid ='+str(chanId))
sys.stdout.write("%i channels updated\n"%len(channels))

#Verifie que tous les xmltvid sont utilisé
for xmltvChannel in xmltvTree.findall('channel') :
    xmltvid = xmltvChannel.attrib['id']
    xmltvChanName = unicode(xmltvChannel.find('display-name').text)
    cr.execute('SELECT COUNT(*) FROM channel WHERE xmltvid = "'+xmltvid+'"')
    if not (cr.fetchone()[0] != 0) :
        sys.stdout.write("Channel %s (%s) unused in mythtv.\n"%(xmltvChanName,xmltvid))
        cr.execute('SELECT chanid,name FROM channel WHERE xmltvid = ""' + whereClause)
        channels = []
        for chanId,chanName in cr.fetchall() :
            if moduleLevenshtein :
                channels.append((levenshtein(re.sub(r'(( HD)|(HD)|( [(].*[)])){0,1}$','',chanName),xmltvChanName),chanId,chanName))
            else :
                channels.append((chanId,chanId,chanName))
        sys.stdout.write("     0:  ignore this channel.\n")

        channels = sorted(channels) # on trie les resulats par disance de levenshtein
        noResult = 1
        for lev,chanId,chanName in channels :
            sys.stdout.write("     %i:  %s\n"%(noResult,chanName))
            if noResult > 19 :
                sys.stdout.write("     a:  View all channels without xmltvid.\n")
                break
            noResult += 1


        sys.stdout.write("What channel corresponding? (Separate response by a space)\n")
        uiRepValid = False
        uiRep = [""]
        while not uiRepValid :
            if uiRep[0] == "a" :
                noResult = 1
                for lev,chanId,chanName in channels :
                    sys.stdout.write("     %i:  %s\n"%(noResult,chanName))
                    noResult += 1
            uiRep = raw_input().split()
            uiRepValid = True
            if len(uiRep) == 0 : #reponse vide
                uiRepValid = False
            for idRep in uiRep :
                if not idRep.isdigit() or int(idRep) > noResult or (len(uiRep) > 1 and idRep == "0") :
                    """on vérfie que ce ne sont des chiffres, pas un no de réponse impossible, et qu'on ignore pas"""
                    uiRepValid = False

        if uiRep[0] != "0" :
            chanQuery = []
            for idRep in uiRep :
                chanQuery.append("chanid = %i"%channels[int(idRep)-1][1])
            cr.execute('UPDATE  channel SET xmltvid = "'+xmltvid+'" WHERE '+" OR ".join(chanQuery))

sys.exit(0)
